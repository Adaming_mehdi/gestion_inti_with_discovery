package com.fr.adaming.config;

import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;
import lombok.Data;

//@Configuration	//décommenter cette ligne pour activer la configuration automatique
public class RegisterInDiscovery {

	private static final String DISCOVERY_BASE_URL = "http://localhost:9090/discovery";
	private static final String DISCOVERY_REGISTER_URL = DISCOVERY_BASE_URL + "/register";

	public RegisterInDiscovery(@Value("${server.port}") String serverPort) {
		System.err.println("DEBUG SERVER PORT : " + serverPort);

		RegisterDto thisMS = new RegisterDto("http://localhost:"+serverPort+"/agence", "MS-GestionAgence");
		
		RestTemplate rest = new RestTemplate();
		
		String discoveryResponse = rest.postForObject(DISCOVERY_REGISTER_URL, thisMS, String.class);
		
		System.err.println("Response Discovery ====> " + discoveryResponse);
	}
}

@Data
@AllArgsConstructor
class RegisterDto {
	private String addressIp;
	private String microServiceName;
}
