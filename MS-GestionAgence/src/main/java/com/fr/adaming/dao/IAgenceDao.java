package com.fr.adaming.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Agence;

@Repository
public interface IAgenceDao extends JpaRepository<Agence, Long>{

}
