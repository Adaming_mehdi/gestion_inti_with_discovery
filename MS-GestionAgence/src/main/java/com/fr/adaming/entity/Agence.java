package com.fr.adaming.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Agence {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String adresse;
	
	private String nom;
	
	private String nomResponsable;

	public Agence(String adresse, String nom, String nomResponsable) {
		super();
		this.adresse = adresse;
		this.nom = nom;
		this.nomResponsable = nomResponsable;
	}

}
