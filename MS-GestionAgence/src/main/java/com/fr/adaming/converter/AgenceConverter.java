package com.fr.adaming.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.fr.adaming.dto.AgenceDto;
import com.fr.adaming.entity.Agence;

public class AgenceConverter {

	public static Agence convertToEntity(AgenceDto dto) {
		if (dto == null) {
			return null;
		}
		return new Agence(dto.getId(), dto.getAddress(), dto.getLabel(), dto.getManagerName());
	}
	
	public static AgenceDto convertToDto(Agence entity) {
		if(entity == null) {
			return null;
		}
		return new AgenceDto(entity.getId(), entity.getAdresse(), entity.getNom(), entity.getNomResponsable());
	}

	public static List<Agence> convertToEntity(List<AgenceDto> dtos) {
		if (dtos == null) {
			return null;
		}
		
		return dtos.stream().map(AgenceConverter::convertToEntity).collect(Collectors.toList());
	}
	
	public static List<AgenceDto> convertToDto(List<Agence> entities) {
		if(entities == null) {
			return null;
		}
		return entities.stream().map(AgenceConverter::convertToDto).collect(Collectors.toList());
	}
}
