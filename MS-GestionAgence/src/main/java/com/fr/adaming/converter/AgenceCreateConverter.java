package com.fr.adaming.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.fr.adaming.dto.AgenceCreateDto;
import com.fr.adaming.entity.Agence;

public class AgenceCreateConverter {

	public static Agence convertToEntity(AgenceCreateDto dto) {
		if (dto == null) {
			return null;
		}
		return new Agence(dto.getAddress(), dto.getLabel(), dto.getManagerName());
	}
	
	
	public static AgenceCreateDto convertToDto(Agence entity) {
		if(entity == null) {
			return null;
		}
		return new AgenceCreateDto(entity.getAdresse(), entity.getNom(), entity.getNomResponsable());
	}
	

	public static List<Agence> convertToEntity(List<AgenceCreateDto> dtos) {
		if (dtos == null) {
			return null;
		}
		return dtos.stream().map(AgenceCreateConverter::convertToEntity).collect(Collectors.toList());
	}
	
	
	public static List<AgenceCreateDto> convertToDto(List<Agence> entities) {
		if(entities == null) {
			return null;
		}
		return entities.stream().map(AgenceCreateConverter::convertToDto).collect(Collectors.toList());
	}

}
