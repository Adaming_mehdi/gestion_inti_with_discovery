package com.fr.adaming.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.dto.ResponseDto;

@RequestMapping(path = "/formation")
public interface IGenericController<S, C> {
	
	@PostMapping
	public ResponseDto<S> create(@RequestBody C dto);

	@GetMapping
	public ResponseDto<List<S>> readAll();

	@GetMapping(path = "/{id}")
	public ResponseDto<S> readById(@PathVariable Long id);
	
	@PutMapping
	public ResponseDto<S> update(@RequestBody S dto);
	
	@DeleteMapping(path = "/{id}")
	public ResponseDto<String> deleteById(@PathVariable Long id);
}
