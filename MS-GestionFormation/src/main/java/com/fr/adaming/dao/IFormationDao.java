package com.fr.adaming.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Formation;

@Repository
public interface IFormationDao extends JpaRepository<Formation, Long>{

	List<Formation> findByDateDebut(LocalDate dateDebut);
	
	Formation findFirstByOrderByDateDebutDesc();
	

}
