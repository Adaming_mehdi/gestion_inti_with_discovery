package com.fr.adaming.converter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import com.fr.adaming.dto.FormationDto;
import com.fr.adaming.entity.Formation;

public class FormationConverter {

	public static Formation convertToEntity(FormationDto dto) {
		if (dto == null) {
			return null;
		}
		return new Formation(dto.getLabel(), LocalDate.parse(dto.getStartDate()), LocalDate.parse(dto.getEndDate()), dto.getNomFormateur());
	}
	
	public static FormationDto convertToDto(Formation entity) {
		if(entity == null) {
			return null;
		}
		return new FormationDto(entity.getId(), entity.getLabel(), entity.getDateDebut().toString(), entity.getDateFin().toString(), entity.getNomFormateur());
	}

	public static List<Formation> convertToEntity(List<FormationDto> dtos) {
		if (dtos == null) {
			return null;
		}
		
		return dtos.stream().map(FormationConverter::convertToEntity).collect(Collectors.toList());
	}
	
	public static List<FormationDto> convertToDto(List<Formation> entities) {
		if(entities == null) {
			return null;
		}
		return entities.stream().map(FormationConverter::convertToDto).collect(Collectors.toList());
	}
}
