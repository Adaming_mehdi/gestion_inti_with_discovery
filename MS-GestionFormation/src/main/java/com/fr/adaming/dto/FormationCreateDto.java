package com.fr.adaming.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FormationCreateDto {

	private String label;

	@ApiModelProperty(example = "2020-11-30")
	private String startDate;


	@ApiModelProperty(example = "2020-12-30")
	private String endDate;

	private String nomFormateur;
}
