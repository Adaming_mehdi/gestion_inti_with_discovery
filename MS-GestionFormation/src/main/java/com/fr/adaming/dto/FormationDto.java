package com.fr.adaming.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FormationDto {
	
	private Long id;

	private String label;

	private String startDate;

	private String endDate;

	private String nomFormateur;
}
