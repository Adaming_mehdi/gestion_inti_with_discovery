package com.fr.adaming.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fr.adaming.controller.IGenericController;
import com.fr.adaming.converter.UserConverter;
import com.fr.adaming.converter.UserCreateConverter;
import com.fr.adaming.dao.IUserDao;
import com.fr.adaming.dto.FormationDto;
import com.fr.adaming.dto.RegisterDto;
import com.fr.adaming.dto.ResponseDto;
import com.fr.adaming.dto.UserCreateDto;
import com.fr.adaming.dto.UserDto;
import com.fr.adaming.entity.User;

@RestController
public class UserController implements IGenericController<UserDto, UserCreateDto>{

	@Autowired
	private IUserDao dao;
	
	private RestTemplate rest = new RestTemplate();

	private static final String DISCOVERY_BASE_URL = "http://localhost:9090/discovery";
	
	private static RegisterDto FORMATION_MICRO_SERVICE = null;

	@Override
	public ResponseDto<UserDto> create(UserCreateDto dto) {
		// TODO catcher les erreurs
		FORMATION_MICRO_SERVICE = rest.getForObject(DISCOVERY_BASE_URL+"/MS-GestionFormation", RegisterDto.class);
		
		if(FORMATION_MICRO_SERVICE != null) {
//			rest.exchange(FORMATION_MICRO_SERVICE.getAddressIp(), HttpMethod.GET, null, new ParameterizedTypeReference<List<FormationDto>>(){});
			FormationDto formation = rest.getForObject(FORMATION_MICRO_SERVICE.getAddressIp()+"/Lyon", FormationDto.class);
			User entity  = UserCreateConverter.convertToEntity(dto);
			entity.setFormationUrl(FORMATION_MICRO_SERVICE.getAddressIp()+"/"+formation.getId());
			User savedEntity = dao.save(entity);
			return new ResponseDto<UserDto>("CREATE USER: SUCCESS AFFECTED TO COURSE SESSION", UserConverter.convertToDto(savedEntity), false);
		}else {
			return new ResponseDto<UserDto>("CREATE USER: SUCCESS BUT NOT AFFECTED TO ANY COURSE SESSION", UserConverter.convertToDto(dao.save(UserCreateConverter.convertToEntity(dto))), true);
		}
	}

	@Override
	public ResponseDto<List<UserDto>> readAll() {
		// TODO catcher les erreurs
		return new ResponseDto<List<UserDto>>("READ ALL : SUCCESS", UserConverter.convertToDto(dao.findAll()), false);
	}

	@Override
	public ResponseDto<UserDto> readById(Long id) {
		// TODO catcher les erreurs
		return new ResponseDto<UserDto>("READ BY ID : SUCCESS", UserConverter.convertToDto(dao.findById(id).get()), false);
	}

	@Override
	public ResponseDto<UserDto> update(UserDto dto) {
		if(dto != null && dto.getId() != null && dao.existsById(dto.getId())) {
			return new ResponseDto<UserDto>("UPDATE : SUCCESS", UserConverter.convertToDto(dao.save(UserConverter.convertToEntity(dto))), false);
		}
		return new ResponseDto<UserDto>("UPDATE : FAIL", null, true);
	}

	@Override
	public ResponseDto<String> deleteById(Long id) {
		// TODO catcher les erreurs
		dao.deleteById(id);
		return new ResponseDto<String>("DELETE : SUCCESS", "User with id "+id+" has been deleted", false);
	}
	
}
