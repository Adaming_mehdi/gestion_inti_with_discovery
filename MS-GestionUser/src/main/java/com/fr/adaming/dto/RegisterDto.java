package com.fr.adaming.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegisterDto {
	private String addressIp;
	private String microServiceName;
}