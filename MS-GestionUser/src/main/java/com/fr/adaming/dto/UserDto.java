package com.fr.adaming.dto;

import com.fr.adaming.enumeration.RoleEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

	private Long id;

	private String lastName;

	private String firstName;

	private String mail;

	private String password;

	private RoleEnum role;

}
